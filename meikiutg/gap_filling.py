from meikiutg.equations import Equation
from meikiutg.orphans import Orphans
from collections import Counter


class GapFilling:
    def __init__(self, model, atlas):
        self.model = [Equation(r) for r in model]
        self.atlas = [Equation(r) for r in atlas]

    def fill_gaps(self):
        orphan_reactants, orphan_products = Orphans(self.model).find_orphans()

        # Reactions dragged from the atlas
        reactions_from_atlas = []

        reactants_completed  = []

        # Finds reactions that contain the given orphan reactants in the group of products
        for orphan in orphan_reactants:
            for reaction in self.atlas:
                if orphan in reaction.products:
                    reactions_from_atlas.append(reaction) 
                    reactants_completed.append(orphan)


        # List of products that already have a reaction filling the gap so that no more than one reaction
        # is dragged from the atlas for a given orphan product
        products_completed = []

        # Finds transport reactions that contain the given orphan reactants in the group of products
        for orphan in orphan_products:
            for reaction in self.atlas:
                if orphan in reaction.reactants:
                    reactions_from_atlas.append(reaction)
                    products_completed.append(orphan)


        # At this point, the array reactions_from_atlas may contain more than one
        # reacton to connect a given metabolite plus reactions tha connect various
        # metabolites
        #
        # The criteria applied for filtering out the reaction is:
        # 1. Sort 'reactions_from_atlas' in descending order by the number of metabolites it completes
        # 2. Take the reaction at the top of the list, move it to a new array and discard the metabolites it completes
        # 3. Take the reaction at the top of the remaining list, if there are still metabolites for that reaction, move it to the new list
        #    otherwise discard tha reaction
        # 4. Repeat the process until there are no remaining reactions in te list or metabolites in the list of orphans.
        
        # [(2, 'h + CoA => Fatthy'), (1, 'H <=> NADH'), (1, 'CoA <=> FAD'), (1, 'CoA + H2O => Linoleico')]
        reactions_from_atlas_grouped = [ (v,k) for k,v in Counter(reactions_from_atlas).items() ]

        # Sorted by frequency and number of compartments, transport reactions have more than one compartment
        reactions_from_atlas_grouped.sort(key=lambda tup: [tup[0], len(tup[1].compartments)])

        reactions_from_atlas_grouped = list(reversed(reactions_from_atlas_grouped))

        # The idea here is to create a tuble that contains the reaction, an array of reactants and an array of products
        # then iterate over the orphans removing from the reaction the given orphan and removing the orphan from the orphans list
        # after the iterations, reactions with higher frequencies and lower remaining metabolites are kept
        thing = []
        for times, reaction in reactions_from_atlas_grouped:
            thing.append({"frequency": times,
                          "reaction": reaction,
                          "reactants": reaction.reactants,
                          "products": reaction.products,
                          "total_compartments": len(reaction.compartments),
                          "reduced_metabolites": 0
                        })

        for reaction in thing:
            self.__discard_orphans_completed_by(reaction, 'products', orphan_reactants)

        for reaction in thing:
            self.__discard_orphans_completed_by(reaction, 'reactants', orphan_products)

        reactions_from_atlas = []
        for r in thing:
            # If the segmented metabolites have fewer elements than the original
            # reaction, means that the reaction completed some orphan metabolites
            # then the reaciton should be selected
            if r['reduced_metabolites'] > 0:
                reactions_from_atlas.append(r['reaction'])

        unique_reactions_from_atlas = list(set(reactions_from_atlas))

        self.reactions_from_atlas = [reaction for reaction in unique_reactions_from_atlas if reaction not in self.model]

        return self.model + self.reactions_from_atlas

    def __discard_orphans_completed_by(self, reaction, field, orphans):
        orphans_to_be_discarded = []
        for orphan in orphans:
            if orphan in reaction[field]:
                reaction[field].remove(orphan)
                reaction['reduced_metabolites'] += 1
                orphans_to_be_discarded.append(orphan)

        for orphan in orphans_to_be_discarded:
            orphans.remove(orphan)

    def __find_transport_reactions_from(self, source):
        transport = [equation for equation in source if equation.is_transport]
        return transport

    def __find_non_transport_reactions_from(self, source):
        non_transport = [equation for equation in source if not equation.is_transport]
        return non_transport
