from meikiutg.metabolites import Metabolite
from meikiutg.equations import Equation
from meikiutg.files import load_rxn_from_file
from openpyxl import load_workbook
from slugify import slugify
import re
import collections
from collections import defaultdict

class Orphans:
    def __init__(self, model):
        self.model = model

    def find_orphans(self):
        disconnected_products = []
        disconnected_reactants = []
    
        for reaction in self.model:
            reactants, products = reaction.reactants, reaction.products

            reactants_list, products_list = self.map_reactants_and_products(self.reactions_different_from(reaction))
            global_list_of_metabolites = list(set(reactants_list+products_list))

            if reaction.reversible:
                # Since metabolites present in a reversible reaction can work as either reactants or products
                # they must be present in the global list of metabolites.
                disconnected_reactants += [reactant for reactant in reactants if reactant not in global_list_of_metabolites]
                disconnected_products += [product for product in products if product not in global_list_of_metabolites]
            else:
                disconnected_reactants += [reactant for reactant in reactants if reactant not in products_list]
                disconnected_products += [product for product in products if product not in reactants_list]

    
        disconnected_products = list(set(disconnected_products))
        disconnected_reactants = list(set(disconnected_reactants))
        return (disconnected_reactants, disconnected_products)

    def reactions_different_from(self, current_reaction):
        return [reaction for reaction in self.model if reaction.full_name != current_reaction.full_name]

    def map_reactants_and_products(self, model):
        reactants_list = []
        products_list = []
        for reaction in model:
            reactants_list += reaction.reactants
            products_list += reaction.products

        return (reactants_list, products_list)
