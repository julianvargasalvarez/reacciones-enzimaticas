from openpyxl import load_workbook

def load_rxn_from_file(filename):
    workbook = load_workbook(filename=filename)
    worksheet = workbook.worksheets[0]
    i = 2
    modelo = []
    while True:
        number =           worksheet["A%s" % i].value
        rxnid =            worksheet["B%s" % i].value
        description =      worksheet["C%s" % i].value
        formula =          worksheet["D%s" % i].value
        ec_number =        worksheet["E%s" % i].value
        gene_association = worksheet["F%s" % i].value
        lower_bound =      worksheet["G%s" % i].value
        upper_bound =      worksheet["H%s" % i].value
        objective =        worksheet["I%s" % i].value
        compartment =      worksheet["J%s" % i].value
        subsystem =        worksheet["K%s" % i].value
        sbo_term =         worksheet["L%s" % i].value
        hepatonet1 =       worksheet["M%s" % i].value
        reactomeid =       worksheet["N%s" % i].value
        keggid =           worksheet["O%s" % i].value
        ehmnid =           worksheet["P%s" % i].value
        biggid =           worksheet["Q%s" % i].value
        references =       worksheet["R%s" % i].value
        comments =         worksheet["S%s" % i].value

        record = {'number': number,
                  'rxnid': rxnid,
                  'description': description,
                  'formula': formula,
                  'ec_number': ec_number,
                  'gene_association': gene_association,
                  'lower_bound': lower_bound,
                  'upper_bound': upper_bound,
                  'objective': objective,
                  'compartment': compartment,
                  'subsystem': subsystem,
                  'sbo_term': sbo_term,
                  'hepatonet1': hepatonet1,
                  'reactomeid': reactomeid,
                  'keggid': keggid,
                  'ehmnid': ehmnid,
                  'biggid': biggid,
                  'references': references,
                  'comments': comments,
                 }

        i += 1

        if not record['rxnid']:
            break
        else:
            modelo.append(record)

    return modelo

def load_rxns(worksheet):
    i = 2
    rxns = []
    while True:
        equation         = worksheet["D%s" % i].value
        gene_assosiation = worksheet["F%s" % i].value

        record = {'equation': equation, 'gene_assosiation': gene_assosiation}
        i += 1

        if not record['equation']:
            break
        else:
            rxns.append(record)
    return rxns


def load_mets(worksheet):
    i = 2
    mets = []
    while True:
        number           = worksheet["A%s" % i].value
        metid            = worksheet["B%s" % i].value
        metname          = worksheet["C%s" % i].value
        unconstrained    = worksheet["D%s" % i].value
        miriam           = worksheet["E%s" % i].value
        composition      = worksheet["F%s" % i].value
        inchl            = worksheet["G%s" % i].value
        compartment      = worksheet["H%s" % i].value
        replacement_id   = worksheet["I%s" % i].value
        lm_id            = worksheet["J%s" % i].value
        systematic_name  = worksheet["K%s" % i].value
        synonyms         = worksheet["L%s" % i].value
        bigg_id          = worksheet["M%s" % i].value
        ehmn_id          = worksheet["N%s" % i].value
        chebi_id         = worksheet["O%s" % i].value
        chebi_id1        = worksheet["P%s" % i].value
        kegg_id          = worksheet["Q%s" % i].value
        hmdb_id          = worksheet["R%s" % i].value
        hepatonet_id     = worksheet["S%s" % i].value

        record = {'number': number,
                  'metid': metid,
                  'metname': metname,
                  'unconstrained': unconstrained,
                  'miriam': miriam,
                  'composition': composition,
                  'inchl': inchl,
                  'compartment': compartment,
                  'replacement_id': replacement_id,
                  'lm_id': lm_id,
                  'systematic_name': systematic_name,
                  'synonyms': synonyms,
                  'bigg_id': bigg_id,
                  'ehmn_id': ehmn_id,
                  'chebi_id': chebi_id,
                  'chebi_id1': chebi_id1,
                  'kegg_id': kegg_id,
                  'hmdb_id': hmdb_id,
                  'hepatonet_id': hepatonet_id,
                 }
        i += 1

        if not record['metid']:
            break
        else:
            mets.append(record)

    return mets


def load_genes(worksheet):
    i = 2
    genes = []
    while True:
        gene_name    = worksheet["B%s" % i].value
        gene_id1     = worksheet["C%s" % i].value
        gene_id2     = worksheet["D%s" % i].value
        short_name   = worksheet["E%s" % i].value
        compartment  = worksheet["F%s" % i].value
        description  = worksheet["G%s" % i].value

        record = {'gene_name': gene_name,
                  'gene_id1': gene_id1,
                  'gene_id2': gene_id2,
                  'short_name': short_name,
                  'compartment': compartment,
                  'description': description}
        i += 1

        if not record['gene_name']:
            break
        else:
            genes.append(record)

    return genes


def load_atlas_from_file(filename):
    workbook = load_workbook(filename=filename)

    rxns  = load_rxns(workbook.worksheets[0])
    mets  = load_mets(workbook.worksheets[1])
    genes = load_genes(workbook.worksheets[4])

    return (rxns, mets, genes)

def write_results(filename, data):
    workbook = load_workbook(filename="data/model-empty.xlsx")
    worksheet = workbook.worksheets[0]
    i = 2

    for record in data:
        worksheet["A%s" % i] = record['number']
        worksheet["B%s" % i] = record['rxnid']
        worksheet["C%s" % i] = record['description']
        worksheet["D%s" % i] = record['formula']
        worksheet["E%s" % i] = record['ec_number']
        worksheet["F%s" % i] = record['gene_association']
        worksheet["G%s" % i] = record['lower_bound']
        worksheet["H%s" % i] = record['upper_bound']
        worksheet["I%s" % i] = record['objective']
        worksheet["J%s" % i] = record['compartment']
        worksheet["K%s" % i] = record['subsystem']
        worksheet["L%s" % i] = record['sbo_term']
        worksheet["M%s" % i] = record['hepatonet1']
        worksheet["N%s" % i] = record['reactomeid']
        worksheet["O%s" % i] = record['keggid']
        worksheet["P%s" % i] = record['ehmnid']
        worksheet["Q%s" % i] = record['biggid']
        worksheet["R%s" % i] = record['references']
        worksheet["S%s" % i] = record['comments']

        i += 1

    workbook.save(filename)

