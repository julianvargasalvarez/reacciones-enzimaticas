from meikiutg.metabolites import Metabolite

class Equation(object):
    def __init__(self, reaction):
        if type(reaction) is not dict:
            raise Exception("Se esperaba un dict")

        self.reaction = reaction

        expression = reaction['formula']

        if expression is not None:
            expression = expression.strip()

        self.full_name = expression

        # "a + b <=> c + d" ==> "<=>"
        self.operator = self.__get_operator__(expression)

        self.reversible = self.operator == "<=>"
        self.not_reversible = not self.reversible

        if expression is not None:
            reactants, products = expression.split(self.operator)
            self.reactants = [Metabolite(r) for r in reactants.split(" + ")]
            self.products  = [Metabolite(p) for p in products.split(" + ")]
        else:
            self.reactants = []
            self.products  = []

        self.metabolites = list(set(self.reactants + self.products))

        if self.reversible:
            self.reactants = self.metabolites
            self.products = self.metabolites

        self.compartments = list(set([m.compartment for m in self.metabolites]))

        self.is_transport = len(self.compartments) > 1

    def __get_operator__(self, expression):
        if expression is None:
          return None

        if "<=>" in expression:
           return "<=>"
        else:
           return "=>"

    def __str__(self):
        return self.full_name

    def __eq__(self, other):
        return self.__str__() == other.__str__()

    def __hash__(self):
        return hash(self.__str__())
