import re

class Metabolite:
    MET_WITHOUT_COEFFICIENT = r"(?:\d+(?:\.\d+)?\s)?(.*)"

    def __init__(self, met_expression):
        # Removes leading and trailing spaces
        met_expression = met_expression.strip()
        self.full_name = met_expression

        # "0.001 H2O+[c]" ==> "H2O+[c]"
        metabolite_without_coefficient = self.__remove_coefficient__(met_expression)
        self.located_name = metabolite_without_coefficient

        # "0.001 H2O+[c]" ==> "0.001"
        self.coefficient = met_expression.replace(metabolite_without_coefficient, "").strip()

        # "H2O+[c]" ==> "[c]"
        self.compartment = met_expression[-3:]

        # Removes the compartment from the metabolite expression without coefficient
        # "H2O+[c]" ==> "H2O+"
        without_compartment = metabolite_without_coefficient.replace(self.compartment, "")

        # "H2O+" ==> "H2O"
        self.name = without_compartment

    def __str__(self):
        return self.located_name

    def __repr__(self):
        return self.located_name

    def __eq__(self, other):
        return self.__str__() == other.__str__()

    def __hash__(self):
        return hash(self.__str__())

    def __remove_coefficient__(self, met_expression):
      parts = re.findall(self.MET_WITHOUT_COEFFICIENT, met_expression)
      if len(parts) == 0: return ""
      return parts[0]
