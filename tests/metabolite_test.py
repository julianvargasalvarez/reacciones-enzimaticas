from unittest import TestCase
from meikiutg.metabolites import Metabolite

class MetaboliteTest(TestCase):
    def test_builds_a_metabolite_from_a_string(self):
        metabolite = Metabolite("0.0001 1-(1,2,3,4,5-pentahydroxypent-1-yl)-1,2,3,4-tetrahydro-beta-carboline-3-carboxylate+[c]")

        # Name without coefficient, compartment.
        self.assertEqual(metabolite.name, "1-(1,2,3,4,5-pentahydroxypent-1-yl)-1,2,3,4-tetrahydro-beta-carboline-3-carboxylate+")

        # Name without coefficient but including ion and compartment
        self.assertEqual(metabolite.located_name, "1-(1,2,3,4,5-pentahydroxypent-1-yl)-1,2,3,4-tetrahydro-beta-carboline-3-carboxylate+[c]")

        # Name including coefficient, ion and compartment
        self.assertEqual(metabolite.full_name, "0.0001 1-(1,2,3,4,5-pentahydroxypent-1-yl)-1,2,3,4-tetrahydro-beta-carboline-3-carboxylate+[c]")

        # stekeometric coefficient
        self.assertEqual(metabolite.coefficient, "0.0001")

        # Returns the compartment
        self.assertEqual(metabolite.compartment, "[c]")
