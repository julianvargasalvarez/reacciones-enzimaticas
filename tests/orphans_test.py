from unittest import TestCase
from meikiutg.orphans import Orphans
from meikiutg.equations import Equation

class OrphansTest(TestCase):
    def test_returns_an_empty_array_when_the_model_is_empty(self):
        model = [
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertEqual(orphan_reactants, [])
        self.assertEqual(orphan_products,  [])

    def test_returns_the_reaction_whene_there_is_only_one_directional_reaction(self):
        model = [
          Equation({'formula': '5 agua+[c] => 3 hidrogeno[c]'}),
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertEqual(orphan_reactants, ['agua+[c]'])
        self.assertEqual(orphan_products,  ['hidrogeno[c]'])


    def test_returns_all_the_metabolites_in_a_reversible_reaction(self):
        model = [
          Equation({'formula': '5 agua+[c] <=> 3 hidrogeno[c]'}),
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertEqual(orphan_reactants, ['agua+[c]', 'hidrogeno[c]'])
        self.assertEqual(orphan_products,  ['agua+[c]', 'hidrogeno[c]'])

    def test_returns_the_metabolites_that_do_not_have_a_match(self):
        model = [
          Equation({'formula': '5 agua+[c]                => 3 hidrogeno[c]'}),
          Equation({'formula': '3 hidrogeno[c]            => 5 agua+[c]'}),
          Equation({'formula': 'pentahidroxypent          => tetrahidro-beta-carboline'}),
          Equation({'formula': 'tetrahidro-beta-carboline => alguna-cosa'}),
          Equation({'formula': 'alguna-cosa              <=> no-mames'}),
          Equation({'formula': 'no-mames                  => se-termino'}),
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertEqual(orphan_reactants, ['pentahidroxypent'])
        self.assertEqual(orphan_products,  ['se-termino'])

    def test_returns_the_metabolites_that_do_not_have_a_match_thing(self):
        # Dont know how to name this example

        model = [
          Equation({'formula': 'PPARA[n] + fatty acid-ligands[n] => activation-ppara[n]'}),
          Equation({'formula': 'activation-ppara[n] => activation-ppara[c]'})
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertItemsEqual(orphan_reactants, ['PPARA[n]', 'fatty acid-ligands[n]'])
        self.assertItemsEqual(orphan_products,  ['activation-ppara[c]'])

    def test_returns_the_metabolites_that_do_not_have_a_match_thing_reversible(self):
        # Dont know how to name this example

        model = [
          Equation({'formula': 'PPARA[n] + fatty acid-ligands[n] <=> activation-ppara[n]'}),
          Equation({'formula': 'activation-ppara[n] => activation-ppara[c]'})
        ]

        orphan_reactants, orphan_products = Orphans(model).find_orphans()

        self.assertItemsEqual(orphan_reactants, ['PPARA[n]', 'fatty acid-ligands[n]'])
        self.assertItemsEqual(orphan_products,  ['activation-ppara[c]', 'PPARA[n]', 'fatty acid-ligands[n]'])
