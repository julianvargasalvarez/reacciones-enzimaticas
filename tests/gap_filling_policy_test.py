from unittest import TestCase
from meikiutg.gap_filling import GapFilling

class GapFillingPolicyTest(TestCase):
    def test_drags_the_first_reaction_found_in_the_atlas(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
            {'formula': 'm[c] => x[c]'},
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[n] => x[c]'},    # Reaction imported from atlas
        ])

    def test_drags_the_reaction_that_fills_gaps_for_most_of_the_disconnected_methabolites(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[c] => x[c]'},
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
            {'formula': 'm[c] => x[c] + CoA[c]'}, # Non transport reaction that fills more than one methabolite
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[c] => x[c] + CoA[c]'},    # Reaction imported from atlas
        ])
