from unittest import TestCase
from meikiutg.gap_filling import GapFilling

# TODO: what happens when more than one filling reactions are found in the atlas?

# For reversible reactions the gap filling process has the following cases:
#
# When the model is reversible and the atlas ireversible
# mrai_orfr: orphan reactant, filler reactant
#   when there is an orphan reactant in the model and there is a filler reactant in the atlas
# mrai_orfp: orphan reactant, filler product
#   when the is an orphan reactant in the model and there is a filler product in the atlas
# mrai_opfr: orphan product, filler reactant
#   when the is an orphan pdoruct in the model and there is a filler reactant in the atlas
# mrai_opfp: orphan product, filler product
#   when there is an orphan product in the model and there is a filler product in the atlas
#
# When the model is irreversible and the atlas reversible
# miar_orfr: orphan reactant, filler reactant
#   when there is an orphan reactant in the model and there is a filler reactant in the atlas
# miar_orfp: orphan reactant, filler product
#   when the is an orphan reactant in the model and there is a filler product in the atlas
# miar_opfr: orphan product, filler reactant
#   when the is an orphan pdoruct in the model and there is a filler reactant in the atlas
# miar_opfp: orphan product, filler product
#   when there is an orphan product in the model and there is a filler product in the atlas
#
# When the model and the atlas are reversible
# mrar_orfr: orphan reactant, filler reactant
#   when there is an orphan reactant in the model and there is a filler reactant in the atlas
# mrar_orfp: orphan reactant, filler product
#   when the is an orphan reactant in the model and there is a filler product in the atlas
# mrar_opfr: orphan product, filler reactant
#   when the is an orphan pdoruct in the model and there is a filler reactant in the atlas
# mrar_opfp: orphan product, filler product
#   when there is an orphan product in the model and there is a filler product in the atlas
#

class GapFillingTest(TestCase):
    def test_includes_transport_reactions_(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[c] => x[c]'},
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[n] => x[c]'},    # Reaction imported from atlas
        ])

    def test_includes_transport_reactions_first(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[c] => x[c]'},
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[n] => x[c]'},    # Reaction imported from atlas
        ])

    def test_includes_any_reaction_that_fills_a_gap_when_there_is_no_transport_reaction(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[c] => x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[c] => x[c]'},    # Reaction imported from atlas
        ])

    def test_does_not_include_reaction_when_there_are_no_matching_metabolites(self):
        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[c] => x[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
        ])

    def test_mrai_orfr(self):
        # model reversible, atlas irreversible
        # orphan reactant, filler reactant

        model = [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] => m[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] <=> y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'x[c] => m[n]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_mrai_orfp(self):
        # model reversible, atlas irreversible
        # orphan reactant, filler product

        model = [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] <=> y[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_mrai_opfr(self):
        # model reversible, atlas irreversible
        # orphan product, filler reactant

        model = [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] => m[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'y[c] <=> x[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'x[c] => m[n]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_mrai_opfp(self):
        # model reversible, atlas irreversible
        # orphan product, filler product

        model = [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'y[c] <=> x[c]'},    # Reaction with dead ends that can be completed by the atlas
            {'formula': 'CoA[c] => H+[n]'}, # Reaction with dead ends that cannot be completed by the atlas
            {'formula': 'm[n] => x[c]'}, # Transport reaction since it includes more than one compartment
        ])


    def test_miar_orfr(self):
        # model irreversible, atlas reversible
        # orphan reactant, filler reactant

        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] <=> m[m]'}, # Transport reaction since it includes more than one compartment
        ]
        filler = GapFilling(model, atlas)
        new_model = filler.fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'x[c] <=> m[m]'}, # Transport reaction since it includes more than one compartment
        ])


    def test_miar_orfp(self):
        # model irreversible, atlas reversible
        # orphan reactant, filler product

        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_miar_opfr(self):
        # model irreversible, atlas reversible
        # orphan product, filler reactant

        model = [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] => y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_miar_opfp(self):
        # model irreversible, atlas reversible
        # orphan product, filler product

        model = [
            {'formula': 'y[c] => x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'y[c] => x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ])


    def test_mrar_orfr(self):
        # model reversible, atlas reversible
        # orphan reactant, filler reactant

        model = [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ])


    def test_mrar_orfp(self):
        # model reversible, atlas reversible
        # orphan reactant, filler product

        model = [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'x[c] <=> y[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ])


    def test_mrar_opfr(self):
        # model reversible, atlas reversible
        # orphan product, filler reactant

        model = [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'x[c] <=> m[n]'}, # Transport reaction since it includes more than one compartment
        ])

    def test_mrar_opfp(self):
        # model reversible, atlas reversible
        # orphan product, filler product

        model = [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
        ]

        atlas = [
            {'formula': 'm[n] => z[c]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ]

        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]

        self.assertItemsEqual(reactions, [
            {'formula': 'y[c] <=> x[c]'},
            {'formula': 'CoA[c] => H+[n]'},
            {'formula': 'm[n] <=> x[c]'}, # Transport reaction since it includes more than one compartment
        ])
