from unittest import TestCase
from meikiutg.metabolites import Metabolite
from meikiutg.equations import Equation

class EquationTest(TestCase):
    def test_builds_an_empty_equation_when_the_formula_is_None(self):
        reaction = {'number': None,
                  'rxnid': '-',
                  'description': None,
                  'formula': None,
                  'ec_number': None,
                  'gene_association': None,
                  'lower_bound': None,
                  'upper_bound': None,
                  'objective': None,
                  'compartment': None,
                  'subsystem': None,
                  'sbo_term': None,
                  'hepatonet1': None,
                  'reactomeid': None,
                  'keggid': None,
                  'ehmnid': None,
                  'biggid': None,
                  'references': None,
                  'comments': None,
                 }
        equation = Equation(reaction)

        self.assertEqual(equation.full_name, None)

        self.assertEqual(equation.operator, None)

        self.assertItemsEqual(equation.compartments, [])

        self.assertTrue(equation.not_reversible)
        self.assertFalse(equation.reversible)

        self.assertEqual(len(equation.metabolites), 0)

        # Since the Metabolite responds to __str__ compares reactants and products to strings
        self.assertItemsEqual(equation.reactants, [])
        self.assertItemsEqual(equation.products, [])

    def test_builds_an_equation_and_its_metabolites_from_a_dictionary(self):
        reaction = {'number': 'Atlas',
                  'rxnid': 'HMR_3905',
                  'description': '',
                  'formula': 'NAD+[c] + ethanol[l] => H+[c] + NADH[s] + acetaldehyde[c]',
                  'ec_number': 'EC:1.1.1.1;EC:1.1.1.71',
                  'gene_association': 'ENSG00000147576;ENSG00000172955',
                  'lower_bound': None,
                  'upper_bound': None,
                  'objective': None,
                  'compartment': 'c',
                  'subsystem': 'Glycolysis / Gluconeogenesis',
                  'sbo_term': 'SBO:0000176',
                  'hepatonet1': 'r0187',
                  'reactomeid': 'REACT_1592',
                  'keggid': 'R00754',
                  'ehmnid': 'R00754C',
                  'biggid': 'ALCD2if',
                  'references': 'PMID:10868354;PMID:12491384;PMID:12818203',
                  'comments': None,
                 }
        equation = Equation(reaction)

        self.assertEqual(equation.full_name, 'NAD+[c] + ethanol[l] => H+[c] + NADH[s] + acetaldehyde[c]')

        self.assertEqual(equation.operator, "=>")

        self.assertItemsEqual(equation.compartments, ["[c]", "[l]", "[s]"])

        self.assertTrue(equation.not_reversible)

        self.assertFalse(equation.reversible)

        self.assertEqual(len(equation.metabolites), 5)

        # Since the Metabolite responds to __str__ compares reactants and products to strings
        self.assertItemsEqual(equation.reactants, ["NAD+[c]", "ethanol[l]"])
        self.assertItemsEqual(equation.products, ["H+[c]", "NADH[s]", "acetaldehyde[c]"])

        self.assertEqual(equation.reaction, reaction)

    def test_when_it_is_reversible_adds_reactants_to_products_and_viceversa(self):
        reaction = {'number': None,
                  'rxnid': '-',
                  'description': None,
                  'formula': 'H2O[c] + RNA[l] <=> AMP[s] + CMP[c]',
                  'ec_number': None,
                  'gene_association': None,
                  'lower_bound': None,
                  'upper_bound': None,
                  'objective': None,
                  'compartment': None,
                  'subsystem': None,
                  'sbo_term': None,
                  'hepatonet1': None,
                  'reactomeid': None,
                  'keggid': None,
                  'ehmnid': None,
                  'biggid': None,
                  'references': None,
                  'comments': None,
                 }
        equation = Equation(reaction)

        self.assertEqual(equation.full_name, "H2O[c] + RNA[l] <=> AMP[s] + CMP[c]")

        self.assertEqual(equation.operator, "<=>")

        self.assertItemsEqual(equation.compartments, ["[c]", "[l]", "[s]"])

        self.assertTrue(equation.reversible)
        self.assertFalse(equation.not_reversible)

        self.assertEqual(len(equation.metabolites), 4)

        # Since the Metabolite responds to __str__ compares reactants and products to strings
        self.assertItemsEqual(equation.reactants, ["H2O[c]", "RNA[l]", "AMP[s]", "CMP[c]"])
        self.assertItemsEqual(equation.products, ["H2O[c]", "RNA[l]", "AMP[s]", "CMP[c]"])
