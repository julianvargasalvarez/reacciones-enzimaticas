import unittest
from tests.metabolite_test import MetaboliteTest
from tests.equation_test import EquationTest
from tests.orphans_test import OrphansTest
from tests.gap_filling_test import GapFillingTest
from tests.gap_filling_policy_test import GapFillingPolicyTest
#from tests.genes_validator import GeneValidator

if __name__ == '__main__':
    unittest.main()
