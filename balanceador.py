import urllib
import urllib2
import re
import time
from bs4 import BeautifulSoup
from openpyxl import load_workbook

def load_data(filename):
    workbook = load_workbook(filename=filename)
    worksheet = workbook.worksheets[0]
    i = 2
    reacciones = []
    while True:
        rxnid       = worksheet["B%s" % i].value
        equation    = worksheet["C%s" % i].value
        original    = worksheet["D%s" % i].value
        record      = (rxnid, equation, original)
        i += 1

        if not equation:
            break
        else:
            reacciones.append(record)

    return reacciones

def balancear(reaction):
    url = 'http://es.webqc.org/balance.php/'
    user_agent = 'Mozilla/5.0 (X11; U; Linux i686) Gecko/20071127 Firefox/2.0.0.11'
    params = urllib.urlencode({'reaction': reaction})
    req = urllib2.Request(url, params)
    req.add_header('User-Agent', user_agent)
    response = urllib2.urlopen(req)
    the_page = response.read()
    soup = BeautifulSoup(the_page)
    cell = soup.findAll("td", {'class', 'center'})[1]
    scripts = [s.extract() for s in cell.findAll('script')]
    anchors = [a.extract() for a in cell.findAll('a')]
    ins = [ins.extract() for ins in cell.findAll('ins')]
    spans = [span.extract() for span in cell.findAll('span')]
    tr = [tr.extract() for tr in cell.findAll('tr')]
    result = cell.get_text().strip().replace(": ", "").replace("\n", "").replace("\n", "")
    result = re.sub(r"Tipo.*", "", result, flags=re.IGNORECASE)
    return result

def write_excel_result(data, filename):
    model = load_workbook('ResultadoBalanceoVacio.xlsx')

    rxns_worksheet = model.worksheets[0]

    for i, (rxnid, equation, original, balanced) in enumerate(data):
        rxns_row = i+2
        rxns_worksheet["B%s" % (rxns_row)] = rxnid
        rxns_worksheet["C%s" % (rxns_row)] = equation
        rxns_worksheet["D%s" % (rxns_row)] = original
        rxns_worksheet["E%s" % (rxns_row)] = balanced

    model.save(filename)

def main():
    unbalanced = load_data('SoloDesbalanceadas.xlsx')
    results = []
    for rxnid, reaction, original in unbalanced:
        result = balancear(reaction)
        results.append( (rxnid, reaction, original, result) )
        print(result)
        time.sleep(1)

    write_excel_result(results, 'ResultadoBalancear.xlsx')

main()
