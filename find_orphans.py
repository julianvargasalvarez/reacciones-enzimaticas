from meikiutg.metabolites import Metabolite
from meikiutg.equations import Equation
from meikiutg.files import load_rxn_from_file
from meikiutg.orphans import Orphans
from openpyxl import load_workbook
from slugify import slugify
import re
import collections
from collections import defaultdict

def main():
    model = load_rxn_from_file('/home/julian/projects/reacciones-enzimaticas/data/Neuron_draft_0.1.xlsx')

    disconnected_products, disconnected_reactants = Orphans(model).find_orphans()

    print("Orphan reactants:")
    for metabolite in disconnected_reactants:
        print(metabolite)

    print("Orphan products:")
    for metabolite in disconnected_reactants:
        print(metabolite)

main()
