from meikiutg.metabolites import Metabolite
from meikiutg.equations import Equation
from meikiutg.orphans import Orphans
from meikiutg.gap_filling import GapFilling
from meikiutg.files import load_rxn_from_file, write_results

def main():
    atlas = load_rxn_from_file('data/HMRdatabase2_00.xlsx')
    genes_assosiation = [r['gene_association'].split(";") for r in atlas if r['gene_association'] is not None]
    genes = [gene for assosiation in genes_assosiation for gene in assosiation]
    unique_genes = list(set(genes))
    
    genes_from_atlas = open('data/HMRdatabase2_00_genes.txt', 'w')
    for gene in unique_genes:
        genes_from_atlas.write("%s\n" % gene)
    genes_from_atlas.close()

main()
