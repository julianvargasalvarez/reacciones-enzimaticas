from openpyxl import load_workbook
from meikiutg.files import load_rxn_from_file, load_atlas_from_file
from meikiutg.equations import Equation
from slugify import slugify
import re

def find_met_details(met, compartment, mets_atlas):
    metabolite = [ m for m in mets_atlas if met==m['metid'] ]
    if len(metabolite)==0:
        m = {'number': '',
         'metid': met,        # metabolite with compartment
         'metname': met.name, # metabolite without compartment
         'unconstrained': None,
         'miriam': None,
         'composition': None,
         'inchl': None,
         'compartment': compartment,
         'replacement_id': None,
         'lm_id': None,
         'systematic_name': None,
         'synonyms': None,
         'bigg_id': None,
         'ehmn_id': None,
         'chebi_id': None,
         'chebi_id1': None,
         'kegg_id': None,
         'hmdb_id': None,
         'hepatonet_id': None,
         }
    else:
        m = metabolite[0]

    return m


def split_into_metabolites(row, mets_atlas):
    equation, compartment = row['formula'], row['compartment']
    metabolites = Equation(equation).metabolites

    mcs = [ find_met_details(met, compartment, mets_atlas) for met in metabolites ]
    return mcs


def split_genes_and_retrieve_details(genes, compartment, genes_atlas):
    if not genes:
        return []
    genes_group = genes.split(";")
    details = []
    for gene in genes_group:
        detail = [g for g in genes_atlas if g['gene_name']==gene]
        details += detail

    return details


def find_genes_association(equation_record, rxns_atlas, genes_atlas):
    genes_association = [ m['gene_assosiation'] for m in rxns_atlas if equation_record['formula']==m['equation'] ]
    compartment = equation_record['compartment']
    # genes_association [u'ENSG00000097021;ENSG00000119673']
    if len(genes_association)==0:
      m = ('',  [])
    else:
      m = (genes_association[0], split_genes_and_retrieve_details(genes_association[0], compartment, genes_atlas))

    return m

def build_metabolites_record(equation_record, mets_atlas, rxns_atlas, genes_atlas):
    try:
        mcs = split_into_metabolites(equation_record, mets_atlas)
    except Exception as e:
        mcs = ("Error: %s" % e.message, equation_record)
        print(mcs)

    try:
        genes = find_genes_association(equation_record, rxns_atlas, genes_atlas)
    except Exception as e:
        genes = ("Error: %s" % e.message, equation_record) 
        print(genes)

    return (equation_record, mcs, genes)

def process_data(data, mets_atlas, rxns_atlas, genes_atlas):
    result = []
    for record in data:
        result.append(build_metabolites_record(record, mets_atlas, rxns_atlas, genes_atlas))

    return result


def write_excel_result(data, filename):
    model = load_workbook('data/FORMATO_MODELO_METABOLITOS.xlsx')

    for i, (row, metabolites, genes) in enumerate(data):
        rxns_row = i+2
        rxns_worksheet = model.worksheets[0]
        rxns_worksheet["A%s" % (rxns_row)] = row['number']
        rxns_worksheet["B%s" % (rxns_row)] = row['rxnid']
        rxns_worksheet["C%s" % (rxns_row)] = row['description']
        rxns_worksheet["D%s" % (rxns_row)] = row['formula']
        rxns_worksheet["E%s" % (rxns_row)] = row['ec_number']
        rxns_worksheet["F%s" % (rxns_row)] = row['gene_association']
        rxns_worksheet["G%s" % (rxns_row)] = row['lower_bound']
        rxns_worksheet["H%s" % (rxns_row)] = row['upper_bound']
        rxns_worksheet["I%s" % (rxns_row)] = row['objective']
        rxns_worksheet["J%s" % (rxns_row)] = row['compartment']
        rxns_worksheet["K%s" % (rxns_row)] = row['subsystem']
        rxns_worksheet["L%s" % (rxns_row)] = row['sbo_term']
        rxns_worksheet["M%s" % (rxns_row)] = row['hepatonet1']
        rxns_worksheet["N%s" % (rxns_row)] = row['reactomeid']
        rxns_worksheet["O%s" % (rxns_row)] = row['keggid']
        rxns_worksheet["P%s" % (rxns_row)] = row['ehmnid']
        rxns_worksheet["Q%s" % (rxns_row)] = row['biggid']
        rxns_worksheet["R%s" % (rxns_row)] = row['references']
        rxns_worksheet["S%s" % (rxns_row)] = row['comments']

    mets_worksheet = model.worksheets[1]

    mets_row = 2
    metabolites = [m[1] for m in data]
    metabolites = sum(metabolites, [])
    unique_metabolites = []
    for m in metabolites:
        if m not in unique_metabolites:
            unique_metabolites.append(m)

    for metabolite in unique_metabolites:
        mets_worksheet["A%s" % mets_row] = metabolite['number']
        mets_worksheet["B%s" % mets_row] = metabolite['metid']
        mets_worksheet["C%s" % mets_row] = metabolite['metname']
        mets_worksheet["D%s" % mets_row] = metabolite['unconstrained']
        mets_worksheet["E%s" % mets_row] = metabolite['miriam']
        mets_worksheet["F%s" % mets_row] = metabolite['composition']
        mets_worksheet["G%s" % mets_row] = metabolite['inchl']
        mets_worksheet["H%s" % mets_row] = metabolite['compartment']
        mets_worksheet["I%s" % mets_row] = metabolite['replacement_id']
        mets_worksheet["J%s" % mets_row] = metabolite['lm_id']
        mets_worksheet["K%s" % mets_row] = metabolite['systematic_name']
        mets_worksheet["L%s" % mets_row] = metabolite['synonyms']
        mets_worksheet["M%s" % mets_row] = metabolite['bigg_id']
        mets_worksheet["N%s" % mets_row] = metabolite['ehmn_id']
        mets_worksheet["O%s" % mets_row] = metabolite['chebi_id']
        mets_worksheet["P%s" % mets_row] = metabolite['chebi_id1']
        mets_worksheet["Q%s" % mets_row] = metabolite['kegg_id']
        mets_worksheet["R%s" % mets_row] = metabolite['hmdb_id']
        mets_worksheet["S%s" % mets_row] = metabolite['hepatonet_id']

        mets_row += 1

    genes_worksheet = model.worksheets[4]

    genes_row = 2
    genes = [m[2][1] for m in data]
    t = [g for g in genes if isinstance(g, tuple)]
    l = [g for g in genes if isinstance(g, list)]
    genes = sum(l, []) + t
    unique_genes = []
    for g in genes:
        if g not in unique_genes:
            unique_genes.append(g)

    for gene in unique_genes:
        genes_worksheet["B%s" % genes_row] = gene['gene_name']
        genes_worksheet["C%s" % genes_row] = gene['gene_id1']
        genes_worksheet["D%s" % genes_row] = gene['gene_id2']
        genes_worksheet["E%s" % genes_row] = gene['short_name']
        genes_worksheet["F%s" % genes_row] = gene['compartment']
        genes_worksheet["G%s" % genes_row] = gene['description']

        genes_row += 1

    comp_worksheet = model.worksheets[2]
    compartment_letters = sum([re.findall("\[\w+\]$", met['metid']) for met in metabolites], [])
    compartment_letters = set(compartment_letters)
    compartment_letters = [c.replace("[", "").replace("]", "") for c in compartment_letters]
    for i, compartment in enumerate(compartment_letters):
        comp_worksheet["B%s" % (i+2)] = compartment
        comp_worksheet["C%s" % (i+2)] = compartment_names[compartment]


    #subs_worksheet = model.worksheets[3]

    #for i, subsystem in enumerate(subsystems(data)):
    #    subs_worksheet["B%s" % (i+2)] = slugify(subsystem).split("-")[0]
    #    subs_worksheet["C%s" % (i+2)] = subsystem


    model.save(filename)


def compartments(data):
    return set([c['compartment'] for c in data])


def subsystems(data):
    return set([subsystem['subsystem'] for subsystem in data])

compartment_names = {
    "s": "Extracellular",
    "p": "Peroxisome",
    "m": "Mitochondria",
    "c": "Cytosol",
    "l": "Lysosome",
    "r": "Endoplasmic reticulum",
    "g": "Golgi apparatus",
    "n": "Nucleus",
    "x": "Boundary",
}

def main():
    data = load_rxn_from_file('disconnected-metabolites.xlsx')
    rxns_atlas, mets_atlas, genes_atlas = load_atlas_from_file('data/HMRdatabase2_00_ESCRITORIO.xlsx')
    data = process_data(data, mets_atlas, rxns_atlas, genes_atlas)

    # creates a file with all the data
    write_excel_result(data, "/tmp/mets/modelo-neurona-diferenciada.xlsx")
    #print("SBMLFromExcel('modelo-neurona-diferenciada.xlsx','modelo-neurona-diferenciada.xml')")

    # creates a file per compartment
    #for compartment in compartments(data):
    #    records = [r for r in data if r[9]==compartment]
    #    write_excel_result(records, "/tmp/mets/%s.xlsx" % compartment)
    #    print("SBMLFromExcel('%s.xlsx','%s.xml')" % (compartment, compartment))

    #for compartment in compartments(data):
    #    records = [r for r in data if r[9]==compartment]

    #    for subsystem in subsystems(records):
    #        records1 = [r for r in records if r[10]==subsystem]
    #        write_excel_result(records1, "/tmp/mets/%s-%s.xlsx" % (compartment, slugify(subsystem)))
    #        print("SBMLFromExcel('%s.xlsx','%s.xml')" % (compartment+"-"+slugify(subsystem), compartment+"-"+slugify(subsystem)))

main()
