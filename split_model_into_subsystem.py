from openpyxl import load_workbook
from slugify import slugify
import re
import collections
from collections import defaultdict

def write_results(filename, data):
    workbook = load_workbook(filename='model-empty.xlsx')
    worksheet = workbook.worksheets[0]
    row = 2

    for record in data:
        rxnid, description, formula, gene_association, gene, protein, subsystem, reversible, lower_bound, upper_bound, objective, confidence, ec_number, compartment, sbo_term, hepatonet1, reactomeid, keegid, ehmnid, biggid, references, comments = record
        rxnid =            worksheet["A%s" % row] = rxnid
        description =      worksheet["B%s" % row] = description
        formula =          worksheet["C%s" % row] = formula
        gene_association = worksheet["D%s" % row] = gene_association
        gene =             worksheet["E%s" % row] = gene
        protein =          worksheet["F%s" % row] = protein
        subsystem =        worksheet["G%s" % row] = subsystem
        reversible =       worksheet["H%s" % row] = reversible
        lower_bound =      worksheet["I%s" % row] = lower_bound
        upper_bound =      worksheet["J%s" % row] = upper_bound
        objective =        worksheet["K%s" % row] = objective
        confidence =       worksheet["L%s" % row] = confidence
        ec_number =        worksheet["M%s" % row] = ec_number
        compartment =      worksheet["N%s" % row] = compartment
        sbo_term =         worksheet["O%s" % row] = sbo_term
        hepatonet1 =       worksheet["P%s" % row] = hepatonet1
        reactomeid =       worksheet["Q%s" % row] = reactomeid
        keegid =           worksheet["R%s" % row] = keegid
        ehmnid =           worksheet["S%s" % row] = ehmnid
        biggid =           worksheet["T%s" % row] = biggid
        references =       worksheet["U%s" % row] = references
        comments =         worksheet["V%s" % row] = comments

        row += 1

    workbook.save(filename)


def load_data(filename):
    workbook = load_workbook(filename=filename)
    worksheet = workbook.worksheets[0]
    i = 2
    modelo = []
    while True:
        rxnid =            worksheet["A%s" % i].value
        description =      worksheet["B%s" % i].value
        formula =          worksheet["C%s" % i].value
        gene_association = worksheet["D%s" % i].value
        gene =             worksheet["E%s" % i].value
        protein =          worksheet["F%s" % i].value
        subsystem =        worksheet["G%s" % i].value
        reversible =       worksheet["H%s" % i].value
        lower_bound =      worksheet["I%s" % i].value
        upper_bound =      worksheet["J%s" % i].value
        objective =        worksheet["K%s" % i].value
        confidence =       worksheet["L%s" % i].value
        ec_number =        worksheet["M%s" % i].value
        compartment =      worksheet["N%s" % i].value
        sbo_term =         worksheet["O%s" % i].value
        hepatonet1 =       worksheet["P%s" % i].value
        reactomeid =       worksheet["Q%s" % i].value
        keegid =           worksheet["R%s" % i].value
        ehmnid =           worksheet["S%s" % i].value
        biggid =           worksheet["T%s" % i].value
        references =       worksheet["U%s" % i].value
        comments =         worksheet["V%s" % i].value

        record = (rxnid, description, formula, gene_association, gene, protein, subsystem, reversible, lower_bound, upper_bound, objective, confidence, ec_number, compartment, sbo_term, hepatonet1, reactomeid, keegid, ehmnid, biggid, references, comments)

        i += 1

        if not formula:
            break
        else:
            modelo.append(record)

    return modelo

def group_model(model):
    group = defaultdict(list)
    for record in model:
        group[record[6]].append(record)

    return group

def main():
    modelo = load_data('Ultimo modelo HMA-HUERFANAS.xlsx')
    group = group_model(modelo)
    for k in group.keys():
        file_name = "Subsystem-%s.xlsx" % slugify(k)
        write_results(file_name, group[k])

main()
