from openpyxl import load_workbook
from slugify import slugify
import re

def is_operator(x):
    return x in ["=>", "<=>", "+"]

def is_coefficient(x):
    return re.match(r"^\d+\.\d+$|^\d+$", x)

def not_operator_or_coefficient(x):
    return not( is_operator(x) or is_coefficient(x) )

def still_in_bounds(i, parts):
    return i < len(parts)

def join_complex_mets(parts):
    """['3', 'COA', '+', 'fatty', 'acid-chylomicron', 'pool[s]']
       ['3', 'COA', '+', 'fatty acid-chylomicron pool[s]']
    """
    new_parts = []
    for i, m in enumerate(parts):
        if not_operator_or_coefficient(m) and still_in_bounds(i+1, parts):
            tmp = parts[i+1]
            if not_operator_or_coefficient(tmp):
                if m:
                    m = m + " " + tmp
                else:
                    m = tmp
                parts[i+1] = ''

        if m: new_parts.append(m)

    if len(new_parts) == len(parts):
        return new_parts
    else:
        return join_complex_mets(new_parts)


def get_parts(equation):
    return equation.split(" ")

def load_data(filename):
    """ Loads data from excel file assuming the active sheet
        has only the three required columns for equation,
        compartment and subsystem.
        returns an arry of tuples of (rxnid, equation, compaprtment, subsystem)
    """
    workbook = load_workbook(filename=filename)
    worksheet = workbook.worksheets[0]
    i = 2
    reacciones = []
    while True:
        rxnid       = worksheet["B%s" % i].value
        equation    = worksheet["D%s" % i].value
        record      = (rxnid, equation)
        i += 1

        if not equation:
            break
        else:
            reacciones.append(record)

    worksheet = workbook.worksheets[1]
    i = 2
    mets = {}
    while True:
        metid       = worksheet["B%s" % i].value
        equation    = worksheet["F%s" % i].value
        i += 1

        if not metid:
            break
        else:
            mets[metid] = equation

    return (reacciones, mets)


def write_excel_result(data, filename):
    model = load_workbook('ResultadoBalanceoVacio.xlsx')

    rxns_worksheet = model.worksheets[0]

    for i, (rxnid, equation, reaction) in enumerate(data):
        rxns_row = i+2
        rxns_worksheet["B%s" % (rxns_row)] = rxnid
        rxns_worksheet["C%s" % (rxns_row)] = equation
        rxns_worksheet["D%s" % (rxns_row)] = reaction

    model.save(filename)

def get_met_molecule(m, metabolitos):
    if m in metabolitos and metabolitos[m]:
        return metabolitos[m]
    else:
        return m

def main():
    reacciones, metabolitos = load_data('RXNS_NEURONA_DIF_ULTIMA_REV.xlsx')
    results = []
    for rid, reaction in reacciones:
        parts_in_equation = get_parts(reaction)
        parts_in_equation = join_complex_mets(parts_in_equation)
        parts_replaced = [get_met_molecule(m, metabolitos) for m in parts_in_equation]
        equation = " ".join(parts_replaced)
        results.append( (rid, equation, reaction) )

    write_excel_result(results, 'ResultadoBalanceo.xlsx')

main()
def probar():
    r = 'heparan sulfate, precursor 1[g]'
    p = get_parts(r)
    j = join_complex_mets(p)
    print(r)
    print(j)

#probar()
