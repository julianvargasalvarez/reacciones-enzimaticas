from meikiutg.metabolites import Metabolite
from meikiutg.equations import Equation
from meikiutg.orphans import Orphans
from meikiutg.gap_filling import GapFilling
from meikiutg.files import load_rxn_from_file, write_results

def main():
    atlas = load_rxn_from_file('data/HMRdatabase2_00.xlsx')

    compartments = ['data/Endoplasmic-reticulum.xlsx',
        'data/Extracellular.xlsx',
        'data/Golgi-apparatus.xlsx',
        'data/Lysosome.xlsx',
        'data/Mitochondria.xlsx',
        'data/Nucleus.xlsx',
        'data/Peroxisome.xlsx',
        'data/cytosol.xlsx',
        'data/reticulo.xlsx']

    for compartment in compartments:
        model = load_rxn_from_file(compartment)
        new_model = GapFilling(model, atlas).fill_gaps()
        reactions = [r.reaction for r in new_model]
        write_results(compartment.replace("data/","results/"), reactions)

        disconnected_reactants, disconnected_products = Orphans([Equation(r) for r in model]).find_orphans()
        orphans_result = open('results/%s.txt' % compartment.replace("data/","").replace(".xlsx",""), 'w')
        orphans_result.write('Disconnected reactants\n')
        for reactant in disconnected_reactants:
            orphans_result.write("%s\n" % reactant)
        orphans_result.write('\n\n')

        orphans_result.write('Disconnected products\n')
        for reactant in disconnected_products:
            orphans_result.write("%s\n" % reactant)
        orphans_result.close()


        disconnected_reactants, disconnected_products = Orphans(new_model).find_orphans()
        orphans_result = open('results/%s_new_model.txt' % compartment.replace("data/","").replace(".xlsx",""), 'w')
        orphans_result.write('Disconnected reactants\n')
        for reactant in disconnected_reactants:
            orphans_result.write("%s\n" % reactant)
        orphans_result.write('\n\n')

        orphans_result.write('Disconnected products\n')
        for reactant in disconnected_products:
            orphans_result.write("%s\n" % reactant)

        orphans_result.close()
main()
