import scrapy


genes = open('data/HMRdatabase2_00_genes.txt','r')
lines = genes.readlines()
genes.close()

urls = ["http://www.proteinatlas.org/%s/tissue/cerebral+cortex" % gene.replace("\n","") for gene in lines]

class ScrapyGene(scrapy.Spider):
    name = 'scrapy_gene'

    # harcoded urls for debugging purposes
    start_urls = ['http://www.proteinatlas.org/ENSG00000157881-PANK4/tissue/cerebral+cortex',
                  'http://www.proteinatlas.org/ENSG00000171503-ETFDH/tissue/cerebral+cortex',
                  'http://www.proteinatlas.org/ENSG00000242366-UGT1A8/tissue/cerebral+cortex',
                  'http://www.proteinatlas.org/ENSG00000101109-STK4/tissue/cerebral+cortex',
                  'http://www.proteinatlas.org/ENSG00000161133-USP41/tissue/cerebral+cortex',
                  'http://www.proteinatlas.org/ENSG00000168038-ULK4/tissue/cerebral+cortex']

    start_urls = urls # Urls mapped from the genes file

    def parse(self, response):
        content = None
        tables = response.css("table.noborder.nowrap")
        if len(tables) == 2:
            content_table = tables[1]
            raw_content = [r.css("::text").extract() for r in content_table.css("tr") if "Neuronal cells:" in r.css("::text").extract()]
            flat_content = [v for data in raw_content for v in data]

            # ["\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t", "Neuronal cells:", "Not detected", "\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"]
            cleaned_data = [t.replace("\n","").replace("\t","") for t in flat_content]
            values = [v for v in cleaned_data if len(v)>0]
            content = "Annotated protein expression " + " ".join(values) if len(values) > 0 else None

        if content is None:
            rows = response.css("table.border.dark tr")
            raw_content = [r.css("::text").extract() for r in rows if "Neuronal cells" in r.css("::text").extract()]
            flat_content = [v for data in raw_content for v in data]
            cleaned_data = [t.replace("\n","").replace("\t","") for t in flat_content]
            values = [v for v in cleaned_data if len(v)>0]
            content = "Antibody staining " + " ".join(values) if len(values) > 0 else None

        if content is None:
            tables = response.css("table.dark")
            content_tables = [t for t in tables if "CEREBRAL CORTEX - Annotated protein expression" in t.css("::text").extract()]
            flat_paragraphs = [p for paragraphs in content_tables for p in paragraphs.css("p")]
            values = [p.css('::text').extract() for p in flat_paragraphs if "CEREBRAL CORTEX - Annotated protein expression" not in p.css("::text").extract()]
            flat_values = [v for value in values for v in value]
            content = " ".join(flat_values)

        if content and "The detailed normal tissue page shows images of the stained tissue" in content:
            content = "Information not available"

        gene_full_name = response.url.split("/")[3]
        gene = gene_full_name.split("-")[0]

        yield {'gene': gene, 'content': content, 'gene1': gene_full_name, 'url': response.url}

        # line for debugging purposes
        # yield {'content': content, 'url': response.url}
